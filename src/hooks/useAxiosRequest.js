import { useState, useEffect } from 'react';
import axios from 'axios';

function useAxiosRequest(url, method, bodyData = null) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function fetchData() {
      try {
        const config = {
          method: method,
          url: url,
          headers: {
            'Content-Type': 'application/json'
          }
        };

        if (bodyData) {
          config.data = bodyData;
        }

        const response = await axios(config);
        setData(response.data);
      } catch (error) {
        setError(error);
      }
    }
    fetchData();
  }, [url, method, bodyData]);

  return { data, error };
}

export default useAxiosRequest;