import React from 'react'
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from 'axios';

const AgregarTarea = () => {
    const validationSchema = Yup.object().shape({
        title: Yup.string().required("Por favor ingresa un título"),
        description: Yup.string().required("Por favor ingresa una descripción"),
    });

    const initialValues = {
        title: "",
        description: "",
    };

    const handleSubmit = async(values, { resetForm }) => {
        try {
            const config = {
              method: 'POST',
              url: 'http://localhost:4000/tareas',
              data: {
                title: values.title,
                status: 'pendiente',
                description: values.description,
                date: Date.now()
              },
              headers: {
                'Content-Type': 'application/json'
              }
            };   
            const response = await axios(config);
            console.log(response);
          } catch (error) {
            console.log(error);
          }
        resetForm();
    };
    

    return(
        <div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched }) => (
          <Form>
            <div className='form-title-input'>
              <label htmlFor="title">Título:</label>
              <Field className='title-input' type="text" id="title" name="title" /><br/>
              <ErrorMessage  name="title" />
            </div>

            <div className='form-description-input'>
              <label htmlFor="description">Descripción:</label><br/>
              <Field className='decription-input' as="textarea" id="description" name="description" /><br/>
              <ErrorMessage name="description" />
            </div>

            <button className='form-submit' type="submit">Enviar</button>
          </Form>
        )}
      </Formik>
    </div>
    )
}


export default AgregarTarea;