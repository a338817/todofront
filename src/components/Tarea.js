import React from 'react'
import Swal from 'sweetalert2';
import axios from 'axios';
const Tarea = ({item}) => {
    const editarTarea = () => {
        Swal.fire({
            title: 'Editar',
            html:
              `<input type="text" id="title" class="swal2-input" value='${item._title}' placeholder="Título">` +
              `<textarea id="content" class="swal2-textarea" placeholder="Descripcion">${item._description}</textarea>`,
            focusConfirm: false,
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            preConfirm: () => {
              const title = Swal.getPopup().querySelector('#title').value;
              const content = Swal.getPopup().querySelector('#content').value;
              return { title: title, content: content };
            }
          }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const config = {
                        method: 'PATCH',
                        url: `http://localhost:4000/tareas/${item._id}`,
                        data: {
                            title: result.value.title,
                            description: result.value.content,
                        },
                        headers: {
                          'Content-Type': 'application/json'
                        }
                      };   
                      const response = await axios(config);
                      //console.log(response);
                } catch (error) {
                    console.log(error);
                }
            }
          });
    }

    const eliminarTarea = () => {
        Swal.fire({
            title: '¿Estás seguro?',
            text: 'Este elemento se eliminará permanentemente',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sí, eliminar',
            cancelButtonText: 'Cancelar'
          }).then(async (result) => {
            if (result.isConfirmed) {
                try {
                    const config = {
                        method: 'DELETE',
                        url: `http://localhost:4000/tareas/${item._id}`,
                        headers: {
                          'Content-Type': 'application/json'
                        }
                      };   
                      const response = await axios(config);
                      //console.log(response);
                } catch (error) {
                    console.log(error);
                }
            
                Swal.fire(
                    'Eliminado',
                    'El elemento ha sido eliminado',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelado',
                    'La eliminación ha sido cancelada',
                    'error'
                )
            }
          })
    }

    return(
        <div>
            <div className='card-title-container'>
                <h3>{item._title}</h3>
                <div className='btns-container'>
                    <button onClick={() => editarTarea()} className='btn-editar'>Editar</button>
                    <button onClick={() => eliminarTarea()} className='btn-eliminar'>Borrar</button>
                </div>
            </div>
            
            <p>{item._description}</p>
        </div>
    )
}

export default Tarea;