import React,{useState, useEffect} from 'react';
import useAxiosRequest from '../hooks/useAxiosRequest';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Tarea from './Tarea';
import axios from 'axios';


const TableCards = () => {
    const [pendientes, setPendientes] = useState([]);
    const [proceso, setProceso] = useState([]);
    const [terminados, setTerminados] = useState([]);
    // 52.87.239.73
    const { data, error } = useAxiosRequest('http://localhost:4000/tareas', 'get');
    useEffect( () => {
        setPendientes([]);
        setProceso([]);
        setTerminados([]);
       if(data){
        data.createTarea.forEach(item => {
            switch (item._status) {
                case "pendiente":

                    setPendientes(lastPendientes => lastPendientes.concat(item));
                    break;
                case "proceso":
                    setProceso(lastPendientes => lastPendientes.concat(item));
                    break;
                case "terminado":
                    setTerminados(lasterminados => lasterminados.concat(item));
                default:
                    break;
            }
        });
       }
    }, [data])

    //console.log(data);
    const isMobile = window.innerWidth <= 768; // asumimos que 768px es el ancho máximo para dispositivos móviles
  
    const droppableDirection = isMobile ? 'horizontal' : 'vertical';

    const generaNuevoEstado = (array,id) => {
        const newEnviado = array.filter(pedidoActual => pedidoActual._id !== id);
        return newEnviado
      }

    const encuentraElemento = (donde,id) => {
        const elemento = donde.filter(pedidoActual => pedidoActual._id === id);
        return elemento[0];
    }

    const cambiaEstado = async (source, destination, id) => {
        let elemento = {};

        try {
            const config = {
                method: 'PATCH',
                url: `http://localhost:4000/tareas/${id}`,
                data: {
                  status: destination.droppableId
                },
                headers: {
                  'Content-Type': 'application/json'
                }
              };   
              const response = await axios(config);
              console.log(response);
        } catch (error) {
            console.log(error);
        }

        switch (source.droppableId) {
            case 'pendiente':
                elemento = encuentraElemento(pendientes,id);
                setPendientes((antPendiente) => generaNuevoEstado(antPendiente,id));
                break;
            case 'proceso':
                elemento = encuentraElemento(proceso,id);
                setProceso((antproceso) => generaNuevoEstado(antproceso,id));
                break;
            case 'terminado':
                elemento = encuentraElemento(terminados,id);
                setTerminados((antTerminados) => generaNuevoEstado(antTerminados,id));
                break;
            default:
                break;
        }
        switch (destination.droppableId) {
            case 'pendiente':
                setPendientes((antPendiente) => [...antPendiente,elemento]);
                break;
            case 'proceso':
                setProceso((antProceso) => [...antProceso,elemento]);
                break;
            case 'terminado':
                setTerminados((antTerminados) => [...antTerminados,elemento]);
                break;
            default:
                break;
        }
    }
    

    if(!data)
        return <div>Cargando...</div>
 
    
    
    return(
        <div className='table-container'>
            {
                data.createTarea.length === 0 ? (
                    <div>No se encontraton registros</div>
                ): (null)
            }
            <DragDropContext onDragEnd={(result) => {
                const {source,destination,draggableId} = result;
                if(!destination){
                  return;
                }
                if (source.index === destination.index && source.droppableId === destination.droppableId) {
                  return;
                }
                cambiaEstado(source,destination,draggableId);

            }}>
                <div className='columnas-arrastrables'>
                    <div className='columna'>
                        <div>
                            <h3>Pendientes</h3>
                        </div>
                        <Droppable droppableId='pendiente' direction={droppableDirection}>
                            {(droppableProvider, snapshopts) => (
                                <ul className="ul-container" {...droppableProvider.droppableProps} ref={droppableProvider.innerRef} >
                                    {pendientes.map((tarea,index) => (
                                        <Draggable key={tarea._id} draggableId={tarea._id} index={index}>
                                            {(draggableProvider) => (
                                                <li className='card'{...draggableProvider.draggableProps} ref={draggableProvider.innerRef} {...draggableProvider.dragHandleProps}>
                                                    <Tarea item={tarea} />
                                                </li>
                                            )}
                                    </Draggable>
                                    ))}
                                    {droppableProvider.placeholder}
                                </ul>
                            )}
                        </Droppable>
                    </div>
                    <div className='columna'>
                        <div>
                            <h3>En proceso</h3>
                        </div>
                        <Droppable droppableId='proceso' direction={droppableDirection}>
                            {(droppableProvider, snapshopts) => (
                                <ul className="ul-container" {...droppableProvider.droppableProps} ref={droppableProvider.innerRef} >
                                    {proceso.map((tarea,index) => (
                                        <Draggable key={tarea._id} draggableId={tarea._id} index={index}>
                                            {(draggableProvider) => (
                                                <li className='card'{...draggableProvider.draggableProps} ref={draggableProvider.innerRef} {...draggableProvider.dragHandleProps}>
                                                <Tarea item={tarea} />
                                                </li>
                                            )}
                                    </Draggable>
                                    ))}
                                    {droppableProvider.placeholder}
                                </ul>
                            )}
                        </Droppable>
                    </div>
                    <div className='columna'>
                        <div>
                            <h3>Terminado</h3>
                        </div>
                        <Droppable droppableId='terminado' direction={droppableDirection}>
                            {(droppableProvider, snapshopts) => (
                                <ul className="ul-container" {...droppableProvider.droppableProps} ref={droppableProvider.innerRef} >
                                    {terminados.map((tarea,index) => (
                                        <Draggable key={tarea._id} draggableId={tarea._id} index={index}>
                                            {(draggableProvider) => (
                                                <li className='card'{...draggableProvider.draggableProps} ref={draggableProvider.innerRef} {...draggableProvider.dragHandleProps}>
                                                <Tarea item={tarea} />
                                            </li>
                                            )}
                                    </Draggable>
                                    ))}
                                    {droppableProvider.placeholder}
                                </ul>
                            )}
                        </Droppable>
                    </div>
                </div>
            </DragDropContext>
            
        </div>
    )
    
}

export default TableCards;