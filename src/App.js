import logo from './logo.svg';
import './App.css';
import React, {useState} from 'react'
import TableCards from './components/TableCards';
import AgregarTarea from './components/AgregarTarea';

function App() {
  const [pantallaAgregar, setPantallaAgregar] = useState(false);
  const [title, setTitle] = useState('Mis tareas')

  const isMobile = window.innerWidth <= 768; // asumimos que 768px es el ancho máximo para dispositivos móviles
  
    const addButton = isMobile ? '+' : 'Agregar'
  return (
    <div className="App">
      <div className='principal-container'>
        {
          pantallaAgregar ? (
            <button className='back-button' onClick={() => setPantallaAgregar(false)} >&lt;</button>
          ) : null
        }
        
        <div className='title-container'>
          <h3>{title}</h3>
        </div>
        <div>
          {
            pantallaAgregar ? (
              <div>
                <AgregarTarea/>
              </div>
            ) : (
              <div>
                <TableCards/>
              </div>
            )
          }
        </div>
        {
          !pantallaAgregar ? (
            <button className='button-add' onClick={() => setPantallaAgregar(true)}>{addButton}</button>
          ) : null
        }
        
      </div>
    </div>
  );
}

export default App;
