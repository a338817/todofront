# Servidor Frontend para TODO-List

Este proyecto es un ejemplo de un servidor Frontend para la aplicación **TODO-List**, realizado con el framework **ReactJS**. Dicha aplicación será desplegada en un **bucket de S3** de AWS como un sitio web estático y se comunicará con el servidor Backend para realizar las operaciones CRUD, por medio de una instancia con **NGINX** en AWS.

## Pantalla principal

Nuestra **pantalla principal** se define desde el archivo [src/App.js](src/App.js), en donde determinamos los componenetes necesarios para la aplicación y mostramos unicamente lo que deseamos que se muestre en la pantalla principal.

## Hook de consulta

El **hook** de consulta se define en el archivo [src/hooks/useAxiosRequest.js](src/hooks/useAxiosRequest.js), en donde se define la función que se encargará de realizar la consulta a la API, y se retorna el estado de la consulta, el resultado de la consulta y la función que realiza la consulta.

```javascript
useEffect(() => {
    async function fetchData() {
      try {
        const config = {
          method: method,
          url: url,
          headers: {
            'Content-Type': 'application/json'
          }
        };

        if (bodyData) {
          config.data = bodyData;
        }

        const response = await axios(config);
        setData(response.data);
      } catch (error) {
        setError(error);
      }
    }
    fetchData();
  }, [url, method, bodyData]);
```

## Componentes

En la carpeta de componentes se encuentran los componentes que se utilizan en la aplicación. Estos se encargan de realiar las peticiones **HTTP** a nuestro servidor **Backend**, y de mostrar los resultados de las peticiones en la pantalla principal. No osbtante estan configuradas para que envien peticiones a un servidor Backend dentro de la misma red, por lo que se debe cambiar la dirección IP del servidor Backend para que se pueda realizar la comunicación a la instanca de **NGINX** en AWS.

Ejemplo con PATCH:

```javascript
const config = {
  method: 'PATCH',
  url: `http://localhost/tareas/${id}`,
  data: {
    status: destination.droppableId
  },
  headers: {
    'Content-Type': 'application/json'
  }
};
```

## Requisitos

Para poder ejecutar este proyecto es necesario tener instalado:

- NodeJS
- NPM
- ReactJS

## Ejecución

Para ejecutar el proyecto, se debe ejecutar el siguiente comando:

```bash
npm run start
```

## Despliegue

Para desplegar el proyecto, se debe ejecutar el siguiente comando:

```bash
npm run build
```

## Funcionamiento

La aplicación es muy sencilla de utilizar. Para agregar tareas es tan simple como hacer click en el botón **"Agregar"** para luego ingresar un título y una descripción de la tarea. 

<img src='./images/add_chore.png' height='300px' width='500px'>
<img src='./images/new_chore.png' height='300px' width='500px'>

Para eliminar una tarea, se debe hacer click en el botón **"Borrar"** de la tarea que se desea eliminar. 

<img src='./images/erase_chore.png' height='300px' width='500px'>

Para editar una tarea, se debe hacer click en el botón **"Editar"** de la tarea que se desea editar, luego se debe ingresar el nuevo título y descripción de la tarea y por último se debe hacer click en el botón "Confirmar".

<img src='./images/edit_chore.png' height='300px' width='500px'>

Asimismo, podemos organizar nuestras tareas en 3 columnas, las cuales son: **"Pendientes", "En proceso" y "Terminadas"**. Para esto, simplemente arrastramos la tarea a la columna que deseemos.

<img src='./images/drag_chore.gif' height='300px' width='500px'>